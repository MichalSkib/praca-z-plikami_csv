import pathlib
import csv
import json
import pickle
import sys


class FileWriterBase:
    ALLOWED_EXTENSIONS = (
        'json',
        'csv',
        'pickle'
    )

    def __init__(self, out_filename, data, path=""):
        self.out_filename = out_filename
        self.path = path
        self.filetype = self.set_filetype()
        self.validated = self.validate()
        self.data = data

    def validate(self):
        if self.filetype not in self.ALLOWED_EXTENSIONS:
            print("Nieobsługiwany format")
            return False
        return True

    def set_filetype(self):
        # return self.filename.split(".")[-1]
        return pathlib.Path(self.out_filename).suffix[1:]


class FileReaderBase:
    ALLOWED_EXTENSIONS = (
        'json',
        'csv',
        'pickle'
    )

    def __init__(self, in_filename, path=""):
        self.in_filename = in_filename
        self.path = path
        self.filetype = self.set_filetype()
        self.validated = self.validate()
        self.data = self.set_data()

    def validate(self):
        if self.filetype not in self.ALLOWED_EXTENSIONS:
            print("Nieobsługiwany format")
            return False
        return True

    def set_filetype(self):
        # return self.filename.split(".")[-1]
        return pathlib.Path(self.in_filename).suffix[1:]

    def get_filepath(self):
        if self.path:
            return f'{self.path}/{self.in_filename}'
        return self.in_filename

    def set_data(self):
        with open(self.get_filepath()) as file:
            if hasattr(self, f'get_data'):
                return getattr(self, f'get_data')(file)
            print(f"Konieczna implementacja metody: get_data na {self}")
            return []

    def make_changes(self):
        changes = sys.argv[1:][2:]
        for change in changes:
            row, column, value = change.split(',')
            self.data[int(row)][int(column)] = value
        return self.data


class CSVReader(FileReaderBase):

    def get_data(self, file):
        data = []
        reader = csv.reader(file)
        for line in reader:
            data.append(line)
        return data


class JSONReader(FileReaderBase):

    def get_data(self, file):
        json_data = json.loads(file.read())
        return json_data


class PICKLEReader(FileReaderBase):

    def set_data(self):
        with open(self.get_filepath(), 'rb') as file:
            if hasattr(self, f'get_data'):
                return getattr(self, f'get_data')(file)
            print(f"Konieczna implementacja metody: get_data na {self}")
            return []

    def get_data(self, file):
        pickle_data = pickle.load(file)
        return pickle_data


class CSVWriter(FileWriterBase):

    def save_file(self):
        with open(self.out_filename, 'w') as file:
            writer_ = csv.writer(file)
            writer_.writerows(self.data)


class JSONWriter(FileWriterBase):

    def save_file(self):
        with open(self.out_filename, 'w') as file:
            json.dump(self.data, file)


class PICKLEWriter(FileWriterBase):

    def save_file(self):
        with open(self.out_filename, 'wb') as f:
            pickle.dump(self.data, f)


type_infile = sys.argv[1].split('.')
type_outfile = sys.argv[2].split('.')

if type_infile[1] == 'csv':
    reader = CSVReader(in_filename=sys.argv[1:][0])
if type_infile[1] == 'json':
    reader = JSONReader(in_filename=sys.argv[1:][0])
if type_infile[1] == 'pickle':
    reader = PICKLEReader(in_filename=sys.argv[1:][0])
reader.make_changes()

if type_outfile[1] == 'csv':
    writer = CSVWriter(out_filename=sys.argv[1:][1], data=reader.data)
if type_outfile[1] == 'json':
    writer = JSONWriter(out_filename=sys.argv[1:][1], data=reader.data)
if type_outfile[1] == 'pickle':
    writer = PICKLEWriter(out_filename=sys.argv[1:][1], data=reader.data)
writer.save_file()
